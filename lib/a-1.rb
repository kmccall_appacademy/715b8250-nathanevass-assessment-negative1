# Given an array of unique integers ordered from least to greatest, write a
# method that returns an array of the integers that are needed to
# fill in the consecutive set.

def missing_numbers(nums)
  (nums.min..nums.max).reject {|n| nums.include?(n)}
end

# Write a method that given a string representation of a binary number will
# return that binary number in base 10.
#
# To convert from binary to base 10, we take the sum of each digit multiplied by
# two raised to the power of its index. For example:
#   1001 = [ 1 * 2^3 ] + [ 0 * 2^2 ] + [ 0 * 2^1 ] + [ 1 * 2^0 ] = 9
#
# You may NOT use the Ruby String class's built in base conversion method.

def base2to10(binary)
  parts = [] 
  binary.to_s.chars.reverse.each_with_index {|n, i| parts << (n.to_i * (2 ** i))}
  parts.reduce(:+)
end


class Hash

  # Hash#select passes each key-value pair of a hash to the block (the proc
  # accepts two arguments: a key and a value). Key-value pairs that return true
  # when passed to the block are added to a new hash. Key-value pairs that return
  # false are not. Hash#select then returns the new hash.
  #
  # Write your own Hash#select method by monkey patching the Hash class. Your
  # Hash#my_select method should have the functionailty of Hash#select described
  # above. Do not use Hash#select in your method.

  def my_select(&prc)
    hash = {}
    self.each do |k, v|
      hash[k] = v if prc.call(k, v)
    end 
    hash 
  end

end

class Hash

  # Hash#merge takes a proc that accepts three arguments: a key and the two
  # corresponding values in the hashes being merged. Hash#merge then sets that
  # key to the return value of the proc in a new hash. If no proc is given,
  # Hash#merge simply merges the two hashes.
  #
  # Write a method with the functionality of Hash#merge. Your Hash#my_merge method
  # should optionally take a proc as an argument and return a new hash. If a proc
  # is not given, your method should provide default merging behavior. Do not use
  # Hash#merge in your method.

  def my_merge(hash, &prc)
    return self.merge(hash) if prc.nil?
    ans = {}
    self.each do |k, v|
      if hash.key?(k)
        ans[k] = prc.call( k, v, hash[k])
      else 
      ans[k] = v
    end 
    end 
    hash.each do |k, v|
      if self.key?(k)
        next 
      else 
        ans[k] = v 
      end 
    end 
    ans
  end

end

# The Lucas series is a sequence of integers that extends infinitely in both
# positive and negative directions.
#
# The first two numbers in the Lucas series are 2 and 1. A Lucas number can
# be calculated as the sum of the previous two numbers in the sequence.
# A Lucas number can also be calculated as the difference between the next
# two numbers in the sequence.
#
# All numbers in the Lucas series are indexed. The number 2 is
# located at index 0. The number 1 is located at index 1, and the number -1 is
# located at index -1. You might find the chart below helpful:
#
# Lucas series: ...-11,  7,  -4,  3,  -1,  2,  1,  3,  4,  7,  11...
# Indices:      ... -5, -4,  -3, -2,  -1,  0,  1,  2,  3,  4,  5...
#
# Write a method that takes an input N and returns the number at the Nth index
# position of the Lucas series.

def lucas_numbers(n)
  start = [2, 1]
  
  return start[n] if n <= 1 && n > -1  
  if n > 0 
      until start.length >= n + 1
        start << start[-1] + start[-2]
      end 
    return start[n]
  else 
    
    star = [-2, -1]
    until star.length >= (n * -1) + 1
      star << star[-1] + star[-2]
    end 
  return star[(n * - 1)] * -1 
  end
end

# A palindrome is a word or sequence of words that reads the same backwards as
# forwards. Write a method that returns the longest palindrome in a given
# string. If there is no palindrome longer than two letters, return false.

def longest_palindrome(string)
  palindromes = []
   string[0..-1].chars.each_with_index do |ch, i|
     string[i..-1].chars.each_with_index do |second, j|
       palindromes << string [i..j - i] if string[i..j - i] == string[i..j - i].reverse 
         
     end 
     
   end 
  longer_than_2 = palindromes.reject {|word| word.length < 3}.sort_by {|w| w.length}
  longer_than_2.empty? ?  false : longer_than_2[-1].length
end
